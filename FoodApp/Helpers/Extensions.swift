//
//  Extensions.swift
//  FoodApp
//
//  Created by nourhan hamada on 03/06/2022.
//

import UIKit
import PKHUD

extension UIViewController {
    func showIndicator() {
        HUD.show(.progress)
    }
    
    func hideIndicator() {
        HUD.hide()
    }
    
    func showAlert(with text: String?) {
        let alert = UIAlertController(title: "", message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    //MARK: - add border
    func addBorderForView (with view: UIView?, color : UIColor , widthBorder : Float , cornorOrNo : Bool  , radius : Float){
        view!.layer.borderColor = color.cgColor
        view!.layer.borderWidth = CGFloat(widthBorder)
        if cornorOrNo {
            view!.layer.cornerRadius = CGFloat(radius)
            view!.layer.masksToBounds = true
        }
    }
    
}

//
//  ReusableView.swift
//  FoodApp
//
//  Created by nourhan hamada on 03/06/2022.
//

import Foundation

protocol ReusableView: AnyObject {}

extension ReusableView {
    
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

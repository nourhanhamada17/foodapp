//
//  HealthFilter.swift
//  FoodApp
//
//  Created by nourhan hamada on 03/06/2022.
//

import Foundation

enum HealthFilter: String {
    static let items: [HealthFilter] = [.all, .lowSugar, .keto, .vegan]
    
    case all = "All"
    case lowSugar = "low-sugar"
    case keto = "keto-friendly"
    case vegan = "vegan"
    
}

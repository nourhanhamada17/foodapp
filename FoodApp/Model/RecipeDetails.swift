//
//  RecipeDetails.swift
//  FoodApp
//
//  Created by nourhan hamada on 03/06/2022.
//

import Foundation

struct RecipeDetails {
    let title: String?
    let image: String?
    let url: String?
    let ingredients: [String]?
    
    init(recipe: Recipe?) {
        self.title = recipe?.label
        self.image = recipe?.image
        self.url = recipe?.url
        self.ingredients = recipe?.ingredientLines
    }
}

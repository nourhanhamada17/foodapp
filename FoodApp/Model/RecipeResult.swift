//
//  RecipeResult.swift
//  FoodApp
//
//  Created by nourhan hamada on 03/06/2022.
//

import Foundation

struct RecipeResult {
    let title: String?
    let image: String?
    let source: String?
    let healthLabels: [String]?
    
    init(recipe: Recipe?) {
        self.title = recipe?.label
        self.image = recipe?.image
        self.source = recipe?.source
        self.healthLabels = recipe?.healthLabels
    }
}

//
//  APIClient.swift
//  FoodApp
//
//  Created by nourhan hamada on 03/06/2022.
//

import Foundation
import Alamofire

struct APIClient {
    func performRequest<T: Decodable>(_ route: APIRouter, completion: @escaping (Result<T, AFError>) -> Void) {
        AF.request(route).responseDecodable(decoder: JSONDecoder()){ (response: DataResponse<T, AFError>) in
            completion(response.result)
        }
    }
}
